import scrapy
import urllib.request
from scrapy.http import Request
import re

class FcbdSpider(scrapy.Spider):
	
	name = "fcbd"
	allowed_domains = ["blogspot.ru"]
	start_urls = [
		'http://fcbdblog.blogspot.ru/2009/01/',
	]

	def parse(self, response):
		for sel in response.xpath('//div[@class="post-body entry-content"]//a[img]/@href'):
			link = sel.extract()	
			yield Request(link, callback=self.save_jpg, dont_filter=True)

	def save_jpg(self, response):
		contenttype = response.headers['Content-Type']		
		path = re.sub('[^.\w]', '', response.url.split('/')[-1])		

		if b'text/html' in contenttype:
			for sel in response.xpath('//img/@src'):
				link = sel.extract();

				with urllib.request.urlopen(link) as res, open('images/'+path, 'wb') as out_file:
					data = res.read()
					out_file.write(data)
		else:
			with open('images/'+path, 'wb') as f:
				f.write(response.body)